﻿using System;

namespace ShiftArrayElements
{
    public static class RecursiveShifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using <see cref="iterations"/> array for getting directions and iterations (odd elements - left direction, even elements - right direction).
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="iterations">An array with iterations.</param>
        /// <returns>An array with shifted elements.</returns>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">iterations array is null.</exception>
        private static int i = 0;
        private static int k = 0;
        private static int j = 0;
        private static int t = 0;

        public static int[] Shift(int[] source, int[] iterations)
        {
            i = 0;
            k = 0;
            j = 0;
            t = 0;
            return ShiftFind(source, iterations);
        }

        public static int[] ShiftFind(int[] source, int[] iterations)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (iterations is null)
            {
                throw new ArgumentNullException(nameof(iterations));
            }

            if (i < iterations.Length)
            {
                if (iterations[i] == 0)
                {
                    i++;
                    return ShiftFind(source, iterations);
                }

                if (i % 2 > 0)
                {
                    if (k < iterations[i])
                    {
                        t = source[^1];
                        j = source.Length - 1;
                        ShiftLeft(source, iterations);
                        source[0] = t;
                        k++;
                        return ShiftFind(source, iterations);
                    }
                    else if (k >= iterations[i])
                    {
                        k = 0;
                    }
                }
                else if (i % 2 == 0 || i == 0)
                {
                    if (k < iterations[i])
                    {
                        t = source[0];
                        j = 0;
                        ShiftRight(source, iterations);
                        source[^1] = t;
                        k++;
                        return ShiftFind(source, iterations);
                    }
                    else if (k >= iterations[i])
                    {
                        k = 0;
                    }
                }

                i++;
                return ShiftFind(source, iterations);
            }

            return source;
        }

        private static int[] ShiftLeft(int[] source, int[] iterations)
        {
            if (j > 0)
            {
                source[j] = source[j - 1];
                j--;
                return ShiftLeft(source, iterations);
            }

            return source;
        }

        private static int[] ShiftRight(int[] source, int[] iterations)
        {
            if (j < source.Length - 1)
            {
                source[j] = source[j + 1];
                j++;
                return ShiftRight(source, iterations);
            }

            return source;
        }
    }
}
