﻿using System;

namespace ShiftArrayElements
{
    public static class RecursiveEnumShifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using directions from <see cref="directions"/> array, one element shift per each direction array element.
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="directions">An array with directions.</param>
        /// <returns>An array with shifted elements.</returns>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">directions array is null.</exception>
        /// <exception cref="InvalidOperationException">direction array contains an element that is not <see cref="Direction.Left"/> or <see cref="Direction.Right"/>.</exception>
        private static int i = 0;

        public static int[] Shift(int[] source, Direction[] directions)
        {
            i = 0;
            return ShiftFind(source, directions);
        }

        public static int[] ShiftFind(int[] source, Direction[] directions)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (directions is null)
            {
                throw new ArgumentNullException(nameof(directions));
            }

            if (i < directions.Length)
            {
                switch (directions[i])
                {
                    case Direction.Right:
                        {
                            int t = source[^1];
                            Array.Copy(source, 0, source, 1, source.Length - 1);
                            source[0] = t;
                            break;
                        }

                    case Direction.Left:
                        {
                            int t = source[0];
                            Array.Copy(source, 1, source, 0, source.Length - 1);
                            source[^1] = t;
                            break;
                        }

                    default:
                        throw new InvalidOperationException();
                }

                i++;

                return ShiftFind(source, directions);
            }

            return source;
        }
    }
}
